import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {User} from "../models/user";

@Injectable({
  providedIn: 'root',
})
export class UserService {

  baseUrl: string = "https://658449be4d1ee97c6bcf6af9.mockapi.io/Tp";

  constructor(
    private http: HttpClient,
  ) {}

  getAllUser(): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl + "/User");
  }

  getUserById(idOfUserToGet: string): Observable<User> {
    return this.http.get<User>(this.baseUrl + "/User/" + idOfUserToGet);
  }

  updateUser(updatedUser: User): Observable<User> {
    return this.http.put<User>(this.baseUrl + "/User/" + updatedUser.userId, updatedUser);
  }

  deleteUser(idOfUserToDelete: string): Observable<User> {
    return this.http.delete<User>(this.baseUrl + "/User/" + idOfUserToDelete);
  }

  addUser(newUser: User): Observable<User> {
    return this.http.post<User>(this.baseUrl + "/User", newUser);
  }
}
