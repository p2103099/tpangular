import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../services/user.service";
import {User} from "../../models/user";
import {Router} from "@angular/router";

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.less']
})
export class AddUserComponent implements OnInit {

  userForm!: FormGroup;


  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private router: Router) {
  }

  ngOnInit(): void {

    this.userForm = this.formBuilder.group({
      nom: [null, [Validators.required]],
      occupation: [null, [Validators.required]],
      mail: [null, [Validators.required, Validators.email]],
      bio: [null, [Validators.required]]
    })
  }


  addSubmit(): void {
    const newUser = new User(
      "",
      this.userForm.get("nom")?.value,
      this.userForm.get("occupation")?.value,
      this.userForm.get("mail")?.value,
      this.userForm.get("bio")?.value
    )

    this.userService.addUser(newUser).subscribe( () => {
      // On reva à la page d'accueil que quand le newUser a bien été ajouté
      // pour être sûr que il soit bien inclus dès que on va re-récupérer tous les users
      this.router.navigateByUrl("/users");
    });
  }


  goToHome() {
    this.router.navigateByUrl("/users")
  }
}
