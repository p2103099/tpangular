import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {lastValueFrom, Observable} from "rxjs";
import {User} from "../../models/user";
import {UserService} from "../../services/user.service";
import {Router} from "@angular/router";
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {users} from "../../data/userData";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.less']
})
export class UserListComponent implements OnInit, AfterViewInit{

  users: User[] = [];

  tableColumns: string[] = [];

  source!: MatTableDataSource<User>;
  @ViewChild("paginator") paginator!: MatPaginator;

  ngAfterViewInit() {

  }

  constructor(private userService: UserService,
              protected router: Router) {
  }

  async ngOnInit(): Promise<void> {
    await this.initUsers();

    this.tableColumns = ['name', 'occupation', 'email', 'bio', 'details', 'update', 'delete'];

  }

  async initUsers() {
    const usersObservable =  await this.userService.getAllUser().subscribe((usersObs) => {
      this.users = usersObs;
      this.source = new MatTableDataSource(this.users);
      this.source.paginator = this.paginator;
    });
  }

  async handleUserDelete(userId: string): Promise<void> {
    this.userService.deleteUser(userId).subscribe(async () => {
      await this.initUsers();
    });
  }

  goToAddUser() {
    this.router.navigateByUrl("/add");
  }
}
