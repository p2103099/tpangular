import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../services/user.service";
import {User} from "../../models/user";
import {lastValueFrom} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.less']
})
export class UpdateUserComponent implements OnInit {

  userId!: string;
  user!: User;

  userForm!: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private activatedRoute: ActivatedRoute,
              private router: Router) {
  }

  async ngOnInit() {
    this.userId = this.activatedRoute.snapshot.params["id"];
    this.user = await lastValueFrom(this.userService.getUserById(this.userId));

    this.userForm = this.formBuilder.group({
      nom: [this.user.name, [Validators.required]],
      occupation: [this.user.occupation, [Validators.required]],
      mail: [this.user.mail, [Validators.required, Validators.email]],
      bio: [this.user.bio, [Validators.required]]
    })
  }

  updateSubmit(): void {
    const updatedUser = new User(
      this.userId,
      this.userForm.get("nom")?.value,
      this.userForm.get("occupation")?.value,
      this.userForm.get("mail")?.value,
      this.userForm.get("bio")?.value
    )

    this.userService.updateUser(updatedUser).subscribe(() => {
      this.router.navigateByUrl("/users");
    });
  }


  goToHome() {
    this.router.navigateByUrl("/users")
  }
}
