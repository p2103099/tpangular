import {Component, Input, OnInit} from '@angular/core';
import {User} from "../../models/user";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../../services/user.service";
import {lastValueFrom} from "rxjs";

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.less']
})
export class UserDetailsComponent implements OnInit{

  userId!: string;
  user!: User;

  constructor(private userService: UserService,
              private activatedRoute: ActivatedRoute,
              private router: Router) {}

  async ngOnInit() {
    this.userId = this.activatedRoute.snapshot.params["id"];
    this.user = await lastValueFrom(this.userService.getUserById(this.userId));
  }

  goToHome() {
    this.router.navigateByUrl("/users")
  }
}
