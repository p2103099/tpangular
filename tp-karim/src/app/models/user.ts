export class User {
  userId?: string;
  name!: string;
  occupation!: string;
  mail!: string;
  bio!: string;

  constructor(userId: string, name: string, occupation: string, mail: string, bio: string) {
    this.userId = userId;
    this.name = name;
    this.occupation = occupation;
    this.mail = mail;
    this.bio = bio;
  }
}
